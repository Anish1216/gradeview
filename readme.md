# GradeView

## Authors
- Anish Krishnan

## Overview
This is a simple program one can use to manage/calculate grades for different classes. A user can add a course, add different grades for that course (as well as score received), and calculate his/her overall grade in that class. Professors can also use something like this to assign grades to their students.

This program covers some of the basic elements of the Django web app framework.

## Notes
### Current features
- Viewable courses, instructors, and assignments
- Automatic calculation of overall course grades based on individual assignments
- Separation of assignments into different types (homeworks, quizzes, exams, etc.)
- Individual user accounts

### Desired features
- Varied permissions (instructors vs. students)
- GPA calculation