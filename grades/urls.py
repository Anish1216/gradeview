from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required

from . import views

app_name = 'grades'
urlpatterns = [

	# Default folders: registration/*.*, accounts/login/*.*
	url(r'^$', views.BaseView.as_view(), name = 'base'),
	url(r'^login/$', auth_views.login, name = 'login'),	# Set with LOGIN_URL in settings.py
	url(r'^logout/$', auth_views.logout, name = 'logout'),	# TODO: Create template
	
    url(r'^index/$', login_required(views.IndexView.as_view()), name = 'index'),
	url(r'^courses/(?P<slug>[-\w\d]+)/$', login_required(views.CourseView.as_view()), name = 'course'),
	url(r'^g/(?P<slug>[-\w\d]+)/$', login_required(views.EditGradeView.as_view()), name = 'edit-grade'),
	url(r'^add-course/$', login_required(views.AddCourseView.as_view()), name = 'add-course'),
	url(r'^courses/(?P<slug>[-\w\d]+)/add-grade/$', login_required(views.AddGradeView.as_view()), name = 'add-grade'),
	url(r'^g/(?P<slug>[-\w\d]+)/delete/$', login_required(views.DeleteGradeView.as_view()), name = 'delete-grade'),
]
