from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.views import generic

from .models import Course, Grade
from .forms import GradeForm, CourseForm, UserForm


class BaseView(generic.View):

	# If the user is logged in, take him/her to the index page. If not, redirect to the login page.
	def get(self, request):
		if request.user.is_authenticated and request.user.is_active:
			return redirect('grades:index')
		else:
			return redirect('grades:login')


class IndexView(generic.ListView):
	template_name = 'grades/index.html'
	context_object_name = 'course_list'
	
	def get_queryset(self):
		student = self.request.user
		return student.course_set.all()


class CourseView(generic.DetailView):
	model = Course
	template_name = 'grades/course.html'
	
	def get_context_data(self, *args, **kwargs):
		context = super(CourseView, self).get_context_data(*args, **kwargs)
		course = self.get_object()
		all_grades = course.grade_set.all().order_by('date')
		context['grade_list'] = all_grades
		
		assignment_types = ['homeworks', 'quizzes', 'exams', 'projects', 'other']
		for e in enumerate(assignment_types):
			grades = all_grades.filter(type = e[0])	# Retrieves grades by type number
			context[e[1]] = grades

		return context


class EditGradeView(generic.UpdateView):
	model = Grade
	form_class = GradeForm
	template_name = 'grades/edit-grade.html'
	
	def get_success_url(self):
		return reverse('grades:course', args = [self.get_object().course.slug])


class AddGradeView(generic.CreateView):
	model = Grade
	form_class = GradeForm
	template_name = 'grades/edit-grade.html'
	
	def get_success_url(self):
		return reverse('grades:index')


class DeleteGradeView(generic.DeleteView):
	model = Grade
	
	def get_success_url(self):
		return reverse('grades:index')

class AddCourseView(generic.CreateView):
	model = Course
	form_class = CourseForm
	template_name = 'grades/edit-course.html'
	
	def form_valid(self, form):
		form.instance.student = self.request.user
		return super(AddCourseView, self).form_valid(form)	# Must return!

	def get_success_url(self):
		return reverse('grades:index')


class UserFormView(generic.View):
	form_class = UserForm
	template_name = 'grades/signup-form.html'
	
	def get(self, request):
		form = self.form_class(None)
		return render(request, self.template_name, { 'form': form })
		
	def post(self, request):
		form = self.form_class(request.POST)
		self.form_valid(form)
		super(UserFormView, self).post(request)
		
	def form_valid(self, form):
		user = form.save(commit = False)
		username = form.cleaned_data['username']
		password = form.cleaned_data['password']
		
		user.set_password(password)	# Uses the hash value
		user.save()
		
		user_auth = authenticate(username = username, password = password)
		
		if user_auth is not None:
			if user_auth.is_active:
				login(request, user)
				return redirect('grades:index')