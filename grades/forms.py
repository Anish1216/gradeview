from django import forms
from django.utils import timezone
from django.contrib.auth.models import User

from .models import Course, Grade


class GradeForm(forms.ModelForm):
	
	ASSIGNMENT_CHOICES = (
		(0, 'Homework'),
		(1, 'Quiz'),
		(2, 'Exam'),
		(3, 'Project'),
		(4, 'Other'),
		(5, 'Extra Credit'),
	)
	
	date = forms.DateField(
		widget = forms.SelectDateWidget(
			empty_label = ('Year', 'Month', 'Day'),
			years = range(2000, timezone.now().year + 1)
		)
	)
	
	class Meta:
		model = Grade
		fields = ['course', 'date', 'name', 'type', 'total_points', 'points_received']


class CourseForm(forms.ModelForm):
	
	class Meta:
		model = Course
		fields = ['course_id', 'name', 'professor_firstname', 'professor_lastname']


class UserForm(forms.ModelForm):
	
	password = forms.CharField(widget = forms.PasswordInput)
	
	class Meta:
		model = User
		fields = ['username', 'email', 'password']