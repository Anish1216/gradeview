from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

import uuid


class Course(models.Model):
	student = models.ForeignKey(User)
	course_id = models.CharField('Course ID', max_length = 16, unique = True)
	name = models.CharField(max_length = 128)
	slug = models.SlugField(default = 'TEST')
	professor_firstname = models.CharField('Professor\'s first name', max_length = 64)
	professor_lastname = models.CharField('Professor\'s last name', max_length = 128)
	
	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		temp_id = self.course_id
		temp_id.replace(' ', '')
		self.slug = slugify(temp_id).upper()
		super(Course, self).save(*args, **kwargs)

	def get_grade(self):
		points = 0
		total = 0
		if not self.grade_set.all():
			return 0
		
		for grade in self.grade_set.all():
			points += grade.points_received
			total += grade.total_points
		
		return round((points / total) * 100, 1)


class Grade(models.Model):
	course = models.ForeignKey(Course, on_delete = models.CASCADE)
	ASSIGNMENT_CHOICES = (
		(0, 'Homework'),
		(1, 'Quiz'),
		(2, 'Exam'),
		(3, 'Project'),
		(4, 'Other'),
	)
	
	name = models.CharField(max_length = 64)
	slug = models.SlugField()
	type = models.IntegerField(choices = ASSIGNMENT_CHOICES, default = 0)
	date = models.DateField(auto_now = False, auto_now_add = False, null = True, blank = True)
	total_points = models.IntegerField()
	points_received = models.IntegerField()
	
	def __str__(self):
		return '{0} - {1}'.format(self.course.name, self.name)

	def save(self, *args, **kwargs):
		exists = Grade.objects.filter(id = self.id)
		if not exists:
			uid = uuid.uuid4()
			self.slug = slugify(str(uid))
		super(Grade, self).save(*args, **kwargs)

	def get_grade(self):
		return round((self.points_received / self.total_points) * 100, 1)